
.PHONY: build
build: cplx.o
	
cplx.o: cplx.cc
	g++ -Wall cplx.cc -o $@

.PHONY: run
run: build
	./cplx.o

.PHONY: clean
clean:
	rm cplx.o
