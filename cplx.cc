#include <iostream>

#define TEST(t) std::cout << #t << " " << (t ? "succeed" : "failed") << std::endl
#define ASSERT(t,str) if(!(t)) {\
		std::cout << str << " --- ";\
		return 0;\
	}

// a + bi
//template <T>
class cplx{

	public:
	cplx (const int& in_a = 0, const int& in_b = 0): a(in_a), b(in_b) {}
	
	int& real() {return a;}
		
	int& imag() {return b;}
		
	int abs_squared() const {
		return a*a + b*b;
	}

	bool operator==(const cplx& other) const {
		return (a == other.a)&&(b == other.b);
	}

	cplx operator+(const cplx& other) const {
 		return cplx(a + other.a, b + other.b);
	}

	void operator+=(const cplx& other){
 		a += other.a;
		b += other.b;
	}

	cplx operator-() const {
 		return cplx(-a, -b);
	}

	cplx operator-(const cplx& other) const {
 		return (*this) + (-other);
	}

	void operator-=(const cplx& other){
 		a -= other.a;
		b -= other.b;
	}

	cplx operator*(const int& other) const {
		return cplx(a * other, b * other);
	}

	cplx operator*(const cplx& other) const {
 		return cplx( (a * other.a) - (b * other.b), (a * other.b) + (b * other.a) );
	}

	void operator*=(const cplx& other){
 		int a_tmp = (a * other.a) - (b * other.b);
		b = (a * other.b) + (b * other.a);
		a = a_tmp;
	}

	//This is conjugation!
	cplx operator~() const {
		return cplx(a, -b);
	}

	cplx operator/(const int& other) const {
		return cplx(a / other, b / other);
	}

	cplx operator/(const cplx& other) const {
		return ( (*this) * (~other) ) / other.abs_squared();
	}

	void operator/=(const cplx& other){
		int absqrd = other.abs_squared();
 		int a_tmp = ((a * other.a) + (b * other.b))/absqrd;
		b = (-(a * other.b) + (b * other.a))/absqrd;
		a = a_tmp;
	}

	private:
	int a;		//maybe all should be float?
	int b;
};

std::ostream& operator<<(std::ostream& out, cplx& c){
	return out << c.real() << " + " << c.imag() << "i";
}

bool test_cplx_add() {
	cplx c1(1, -2);
	cplx c2(4, 3);
	ASSERT((cplx(5, 1) == (c1 + c2)), "Addition fails");	//works fine with const +
	ASSERT(((c1 + c2) == cplx(5, 1)), "Addition fails");	//fails when using const +
	c2 += c1;
	ASSERT((c2) == cplx(5, 1), "Addition increment fails" );
	return 1;
}

bool test_cplx_eq() {
	cplx c1(1, -2);
	cplx c2(1, 3);
	ASSERT(!(c1 == c2), "Non equal values return equal");
	ASSERT((c1 == c1), "Value not equal to itself");
	return 1; 
}

bool test_cplx_minus(){
	cplx c1(1, -2);
	ASSERT((-c1 == cplx(-1, 2)), "Negation fails");
	return 1;
}

bool test_cplx_sub() {
	cplx c1(1, -2);
	cplx c2(4, 3);
	ASSERT((c1 - c2) == cplx(-3, -5), "Substraction fails");
	c2 -= c1;
	ASSERT((c2) == cplx(3, 5), "Substraction increment fails" );
	return 1;
}

bool test_cplx_mul() {
	cplx c1(1, -2);
	cplx c2(4, 3);
	ASSERT((c1 * c2) == cplx(10, -5), "Multipication fails");

	c2 *= c1;
	ASSERT((c2) == cplx(10, -5), "Multiplication increment fails" );
	return 1;
}

bool test_cplx_div() {
	cplx c1(10, -5);
	cplx c2(4, 3);
	ASSERT((c1 / c2) == cplx(1, -2), "Division fails");
	c1 /= c2;
	ASSERT(((c1) == cplx(1, -2)), "Division increment fails" );
	return 1;
}

int main() {
	cplx c1(1, -2);
	cplx c2(4, 3);
	cplx c3;
	TEST(test_cplx_eq());
	TEST(test_cplx_add());
	TEST(test_cplx_minus());
	TEST(test_cplx_sub());
	TEST(test_cplx_mul());
	TEST(test_cplx_div());
	TEST(0);

	//print_test();	

	return 0;
}
